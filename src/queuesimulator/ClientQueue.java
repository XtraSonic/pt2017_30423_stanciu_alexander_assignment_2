/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queuesimulator;

import java.util.LinkedList;

/**
 *
 * @author XtraSonic
 */

//
public class ClientQueue extends Thread {

    private int queueTime;
    private final LinkedList<Client> queue;
    private final int queueID;
    private final int closeTime;
    private final Object lock;

    public ClientQueue(int id, int close, Object lock)
    {
        this.queue = new LinkedList<>();
        this.queueTime = 0;
        this.queueID = id;
        this.closeTime = close;
        this.lock = lock;
    }

    public synchronized void addClient(Client client)
    {
        this.queue.addLast(client);
        this.queueTime += client.getServingTime();
       // System.out.println("Client " + client.getID() + " has been added to the queue " + this.queueID+ " "+client.getServingTime());
        SimGUI.textTF.append("Client " + client.getID() + " has been added to the queue " + this.queueID+ " with serving time="+client.getServingTime()+"\n");
        notifyAll();
    }

    //removes served client from the list and returns the waiting time of that client
    private synchronized void removeClient()
    {
        if (this.queue.peekFirst()!=null&&this.queue.peekFirst().serving())
        {
            Client client = this.queue.removeFirst();
            //this.totalWaitingTime += currentTime - client.getArrivalTime();
            //System.out.println("Client " + client.getID() + " has been served and leaves queue " + this.queueID);
            SimGUI.textTF.append("Client " + client.getID() + " has been served and leaves queue " + this.queueID+"\n");
            
        }
        notifyAll();
    }

    public int getQueueTime()
    {
        return this.queueTime;
    }

    public int getQueueID()
    {
        return queueID;
    }

    @Override
    public void run()
    {
        try
        {
            while (this.closeTime > Store.getTime())
            {
                //System.out.println("Test1 " + queueID);
                synchronized (this.lock)
                {
                    this.lock.wait();
                }
                //System.out.println("Test2 " + queueID);
                this.removeClient();
                if(this.queueTime>0)
                    this.queueTime--;

            }
        }
        catch (InterruptedException ex)
        {
            System.out.println("interupted " + this.queueID);
        }
    }
    
    public int getSize()
    {
        return this.queue.size();
    }
    
    @Override
    public synchronized String toString()
    {
        String result=new String();
        for(Client c:this.queue)
        {
            result=result.concat(c.toString()+" ");
        }
        notifyAll();
        return result;
    }
}
