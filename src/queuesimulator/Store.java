/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queuesimulator;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author XtraSonic
 */
public class Store extends Thread {

    private static final Random rand = new Random();
    private final ClientQueue[] queues;
    private final int nrQueues;
    private final int endSimulation;
    private static int globalTime;
    private final int minArrival;
    private final int maxArrival;
    private int totalClients;
    private final int maxServing;
    private final int minServing;
    private int lastInserted;
    private int peakTime;
    private int peakValue;
    private int totalWaitingTime;
    private int totalServingTime;

    Store(int startSimulation, int endSimulation, int nrQueues, int minArrival, int maxArrival, int minServing, int maxServing)
    {
        globalTime = startSimulation;
        this.endSimulation = endSimulation;
        this.nrQueues = nrQueues;
        queues = new ClientQueue[nrQueues];
        this.peakTime=startSimulation;
        this.maxArrival = maxArrival;
        this.minArrival = minArrival;
        this.maxServing = maxServing;
        this.minServing = minServing;
        this.lastInserted=-minArrival;

    }

    public static int getTime()
    {
        return globalTime;
    }

    @Override
    public void run()
    {
        int min;
        int currentPeak;
        for (int i = 0; i < nrQueues; i++)
        {
            queues[i] = new ClientQueue(i, endSimulation, this);
            queues[i].start();
        }
        try
        {

            while (this.endSimulation >=globalTime)
            {
                sleep(1000);
                min = 0;

                for (int i = 1; i < this.nrQueues; i++)
                {
                    if (queues[i].getQueueTime() < queues[min].getQueueTime())
                    {
                        min = i;
                    }

                }
                if(globalTime-lastInserted>=this.minArrival){
                    if (rand.nextInt(10) == 0||globalTime-lastInserted==this.maxArrival)
                    {
                        Client client =new Client(totalClients, globalTime, minServing, maxServing);
                        this.totalClients++;
                        this.totalWaitingTime+=queues[min].getQueueTime()+client.getServingTime();
                        this.totalServingTime+=client.getServingTime();
                        lastInserted=globalTime;
                        queues[min].addClient(client);
                    }
                    if(globalTime-lastInserted>this.maxArrival)
                        lastInserted=globalTime;
                }
                currentPeak=0;
                for(int i=0;i<nrQueues;i++)
                {
                    currentPeak+=queues[i].getSize();
                }
                if(currentPeak>this.peakValue)
                {
                    this.peakTime=this.globalTime;
                    this.peakValue=currentPeak;
                }
                /*
                for (int i=0;i<this.nrQueues;i++)
                {
                    System.out.print(" Q["+i+"]="+queues[i].getQueueTime());
                }
                System.out.print("\n");*/
                    globalTime++;
                
                //System.out.println(globalTime);
                synchronized (this)
                {
                    notifyAll();

                }
            }
        }
        catch (InterruptedException ex)
        {
            System.out.println("Interupted " + ex.getMessage());
        }
    }
    
    public int getPeakTime()
    {
        return this.peakTime;
    }
    
    public float getWaitingAvrg()
    {
        if(this.totalClients==0)
            return 0;
        return (float)this.totalWaitingTime/this.totalClients;
    }
    public float getServingAvrg()
    {
        if(this.totalClients==0)
           return 0;
        return (float)this.totalServingTime/this.totalClients;
    }
    public String getQueueString(int i)
    {
        return this.queues[i].toString();
    }
    
    public void stopTime()
    {
        globalTime=endSimulation+1;
        for(int i=0;i<nrQueues;i++)
        {
            try {
                queues[i].join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Store.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
