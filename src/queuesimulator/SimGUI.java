/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queuesimulator;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Xtra Sonic
 */
public class SimGUI {//extends Thread {

    private final JTextField minArrTF;
    private final JLabel minArrL;
    private final JTextField minServTF;
    private final JLabel minServL;
    private final JTextField StartTF;
    private final JLabel StartL;
    private final JTextField maxArrTF;
    private final JLabel maxArrL;
    private final JTextField maxServTF;
    private final JLabel maxServL;
    private final JTextField StopTF;
    private final JLabel StopL;
    private final JTextField nrQueuesTF;
    private final JLabel nrQueuesL;
    private final JSplitPane split;
    private JPanel secondPanel;

    private JTextField[] queuesTF;
    private JLabel[] queuesL;
    
    private final JTextField timeTF;
    private final JLabel timeL;
    private final JTextField peakTimeTF;
    private final JLabel peakTimeL;
    private final JTextField avgWaitingTF;
    private final JLabel avgWaitingL;
    private final JTextField avgServingTF;
    private final JLabel avgServingL;
    public static final JTextArea textTF=new JTextArea(25,25);
    public final JScrollPane scroll;
    private final JLabel textL;
    
    private final JButton StartBtn;

    private final JFrame mainFrame;

    private Store s;
    private ThreadGUI t;
    
    public SimGUI() {
        mainFrame = new JFrame();

        //input set
        minArrTF = new JTextField(5);
        minArrTF.setHorizontalAlignment(JTextField.CENTER);
        minArrL = new JLabel("Minimal Arrival Time");
        minServTF = new JTextField(5);
        minServTF.setHorizontalAlignment(JTextField.CENTER);
        minServL = new JLabel("Minimal Serving Time");
        StartTF = new JTextField(5);
        StartTF.setHorizontalAlignment(JTextField.CENTER);
        StartL = new JLabel("Start Time");
        maxArrTF = new JTextField(5);
        maxArrTF.setHorizontalAlignment(JTextField.CENTER);
        maxArrL = new JLabel("Maximum Arrival Time");
        maxServTF = new JTextField(5);
        maxServTF.setHorizontalAlignment(JTextField.CENTER);
        maxServL = new JLabel("Maximum Serving Time");
        StopTF = new JTextField(5);
        StopTF.setHorizontalAlignment(JTextField.CENTER);
        StopL = new JLabel("Stop Time");
        nrQueuesTF = new JTextField(5);
        nrQueuesTF.setHorizontalAlignment(JTextField.CENTER);
        nrQueuesL = new JLabel("Number of queues");
        
        timeTF = new JTextField(5);
        timeTF.setEditable(false);
        timeL=new JLabel("Simulation Time");
        
    
        peakTimeTF= new JTextField(5);
        peakTimeTF.setEditable(false);
        peakTimeL=new JLabel("PeakTime");
        avgWaitingTF= new JTextField(5);
        avgWaitingTF.setEditable(false);
        avgWaitingL=new JLabel("Average Waiting Time");
        avgServingTF= new JTextField(5);
        avgServingTF.setEditable(false);
        avgServingL=new JLabel("Average Serving Time");
        //textTF= new JTextArea(25,25);
        textTF.setEditable(false);
        textL=new JLabel("Log");
        scroll=new JScrollPane(textTF);
        scroll.setAutoscrolls(true);
        
        
        split = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        
        t= new ThreadGUI();
        //Buttons set
        StartBtn = new JButton("Start");
        StartBtn.addActionListener((ActionEvent ae) -> {
            if(t.isAlive())
                t.stopAll();
            t=new ThreadGUI();
            t.start();
        });

        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new GridBagLayout());

        GridBagConstraints constrains = new GridBagConstraints();
        constrains.fill = GridBagConstraints.HORIZONTAL;
        constrains.gridwidth = 1;
        constrains.insets = new Insets(5, 5, 5, 5);
        constrains.anchor = GridBagConstraints.CENTER;
        //constrains.ipadx=200;

        //Input simulation time
        constrains.gridx = 0;
        constrains.gridy = 0;
        inputPanel.add(StartL, constrains);
        constrains.gridx = 1;
        inputPanel.add(StartTF, constrains);
        constrains.gridx = 2;
        inputPanel.add(StopL, constrains);
        constrains.gridx = 3;
        inputPanel.add(StopTF, constrains);

        //Input arrival time
        constrains.gridx = 0;
        constrains.gridy = 1;
        inputPanel.add(minArrL, constrains);
        constrains.gridx = 1;
        inputPanel.add(minArrTF, constrains);
        constrains.gridx = 2;
        inputPanel.add(maxArrL, constrains);
        constrains.gridx = 3;
        inputPanel.add(maxArrTF, constrains);

        //Input serving time
        constrains.gridx = 0;
        constrains.gridy = 2;
        inputPanel.add(minServL, constrains);
        constrains.gridx = 1;
        inputPanel.add(minServTF, constrains);
        constrains.gridx = 2;
        inputPanel.add(maxServL, constrains);
        constrains.gridx = 3;
        inputPanel.add(maxServTF, constrains);

        //Input nr queues
        constrains.gridx = 0;
        constrains.gridy = 3;
        inputPanel.add(nrQueuesL, constrains);
        constrains.gridx = 1;
        inputPanel.add(nrQueuesTF, constrains);
        constrains.gridx = 2;
        constrains.gridwidth = 2;
        inputPanel.add(StartBtn, constrains);

        //set main frame
        split.setTopComponent(inputPanel);
        mainFrame.setContentPane(split);
        mainFrame.pack();
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setVisible(true);
    }

    /*@Override
    public void run() {
        secondPanel = new JPanel();
        s = new Store(Integer.parseInt(StartTF.getText()), Integer.parseInt(StopTF.getText()),
                Integer.parseInt(nrQueuesTF.getText()),
                Integer.parseInt(minArrTF.getText()), Integer.parseInt(maxArrTF.getText()),
                Integer.parseInt(minServTF.getText()), Integer.parseInt(maxServTF.getText()));
        int nrQs = Integer.parseInt(nrQueuesTF.getText());
        queuesTF = new JTextField[nrQs];
        queuesL = new JLabel[nrQs];
        secondPanel.setLayout(new BoxLayout(secondPanel, BoxLayout.Y_AXIS));
        secondPanel.add(timeL);
        secondPanel.add(timeTF);
        
        for (int i = 0; i < nrQs; i++) {
            queuesTF[i] = new JTextField(25);
            queuesTF[i].setEditable(false);
            queuesL[i] = new JLabel("Queue number " + (i + 1));
            secondPanel.add(queuesL[i]);
            secondPanel.add(queuesTF[i]);

        }
        secondPanel.add(peakTimeL);
        secondPanel.add(peakTimeTF);
        secondPanel.add(avgWaitingL);
        secondPanel.add(avgWaitingTF);
        secondPanel.add(avgServingL);
        secondPanel.add(avgServingTF);
        secondPanel.add(textL);
        secondPanel.add(textTF);

        split.setBottomComponent(secondPanel);
        mainFrame.pack();
        s.start();

        while (s.isAlive()) {
            try {
                timeTF.setText(Integer.toString(Store.getTime()));
                synchronized (s) {
                    s.wait();
                }
                avgWaitingTF.setText(Float.toString(s.getWaitingAvrg()));
                avgServingTF.setText(Float.toString(s.getServingAvrg()));
                peakTimeTF.setText(Integer.toString(s.getPeakTime()));
                
                for(int i=0;i<nrQs;i++)
                {
                    queuesTF[i].setText(s.getQueueString(i));
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(SimGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }*/

    public static void main(String args[]) {
        //Store store = new Store(0, 100, 3, 3, 17,1, 5);
        SimGUI s = new SimGUI();
        //store.start();

    }
    

class ThreadGUI extends Thread
{
    public void stopAll()
    {
        if(s.isAlive())
        {
            s.stopTime();
            try {
                s.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(SimGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    @Override
    public void run() {
        secondPanel = new JPanel();
        s = new Store(Integer.parseInt(StartTF.getText()), Integer.parseInt(StopTF.getText()),
                Integer.parseInt(nrQueuesTF.getText()),
                Integer.parseInt(minArrTF.getText()), Integer.parseInt(maxArrTF.getText()),
                Integer.parseInt(minServTF.getText()), Integer.parseInt(maxServTF.getText()));
        int nrQs = Integer.parseInt(nrQueuesTF.getText());
        queuesTF = new JTextField[nrQs];
        queuesL = new JLabel[nrQs];
        secondPanel.setLayout(new BoxLayout(secondPanel, BoxLayout.Y_AXIS));
        secondPanel.add(timeL);
        secondPanel.add(timeTF);
        
        for (int i = 0; i < nrQs; i++) {
            queuesTF[i] = new JTextField(25);
            queuesTF[i].setEditable(false);
            queuesL[i] = new JLabel("Queue number " + (i + 1));
            secondPanel.add(queuesL[i]);
            secondPanel.add(queuesTF[i]);

        }
        secondPanel.add(textL);
        textTF.setText("");
        secondPanel.add(scroll);
        secondPanel.add(peakTimeL);
        peakTimeTF.setText("");
        secondPanel.add(peakTimeTF);
        secondPanel.add(avgWaitingL);
        avgWaitingTF.setText("");
        secondPanel.add(avgWaitingTF);
        secondPanel.add(avgServingL);
        avgServingTF.setText("");
        secondPanel.add(avgServingTF);

        split.setBottomComponent(secondPanel);
        mainFrame.pack();
        s.start();

        while (s.isAlive()) {
            try {
                timeTF.setText(Integer.toString(Store.getTime()));
                synchronized (s) {
                    s.wait();
                }
                avgWaitingTF.setText(Float.toString(s.getWaitingAvrg()));
                avgServingTF.setText(Float.toString(s.getServingAvrg()));
                peakTimeTF.setText(Integer.toString(s.getPeakTime()));
                
                for(int i=0;i<nrQs;i++)
                {
                    queuesTF[i].setText(s.getQueueString(i));
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(SimGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }
}
}
