/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queuesimulator;

import java.util.Random;

/**
 *
 * @author XtraSonic
 */
public class Client {

    private final int clientID;
    private final int arrivalTime;
    private int servingTime;
    //private final int finishTime;

    private static final Random rand = new Random();

    public Client(int id, int time, int minServing, int maxServing) {
        this.clientID = id;
        this.arrivalTime = time;
        if (maxServing != minServing) {
            this.servingTime = (rand.nextInt(maxServing - minServing) + minServing);
        } else {
            this.servingTime = minServing;
        }

    }

    public int getArrivalTime() {
        return this.arrivalTime;
    }

    public int getServingTime() {
        return this.servingTime;
    }

    public int getID() {
        return this.clientID;
    }

    public boolean serving() {
        this.servingTime--;
        return this.servingTime == 0;
    }
    
    @Override
    public String toString()
    {
        return Integer.toString(clientID);
    }
}
